var limit=20;
	
$(document).ready(function() {
setIcons();
});

	 
	$(document).on("keypress", "#search-input", function (e) {
		if (e.keyCode == 13 || e.which == '13') {
		   iniciarBusqueda();
		}
	});

function iniciarBusqueda(){
	var input = $("#search-input").val();
	busqueda = input;
	buscar(input,1);
}

function iniciarBusquedaSolr(){
	var input = $("#search-input").val();
	busqueda = input;
	buscarSolr(input,1);
}

function buscar(query,pagina){
	var start=limit*(pagina-1)+1;
	var dir = '/EntitiesSearchService/client/search?query='+query+'&limit='+limit+'&start='+start;
	location.href = dir;
}

function buscarSolr(query,pagina){
	var start=limit*(pagina-1)+1;
	var dir = '/EntitiesSearchService/client/searchSolr?query='+query+'&limit='+limit+'&start='+start;
	location.href = dir;
}

function mostrarResultado(uri){
	
		window.open('/EntitiesSearchService/entity?subject='+encodeURIComponent(uri));
	
}

function setIcons(){
	var tiposIconos = Object.keys(iconosBusquedaTipos);
	for (var i=0; i<tiposIconos.length;i++){
		var tipo = tiposIconos[i];
		$("."+tipo).addClass(iconosBusquedaTipos[tipo].replace('<i class="fas ','').replace('"></i>',''));
	
	}
	var tiposNombre = Object.keys(tipos);
	for (var i=0; i<tiposNombre.length;i++){
		var tipo = tiposNombre[i];
		
		$("."+tipo.toLowerCase()).parent().prop("title",tipos[tipo]);
	}
}