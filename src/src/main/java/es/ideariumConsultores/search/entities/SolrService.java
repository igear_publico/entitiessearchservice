package  es.ideariumConsultores.search.entities;


import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Service
public class SolrService {
	@Value("${solr.url}")
	String solr_url;	

	Integer numFound;
	String facets;
	
	private final RestTemplate restTemplate;

	private final static Logger log = LoggerFactory.getLogger(SolrService.class.getName());

	@Autowired
	public SolrService( RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();

	}

	public Collection<Entity> search( String texto, int inicio, int total,HashMap<String,TypeConfig> config_types,String[] tipos) throws Exception{

		return search(null,null,texto,inicio,total,config_types,tipos);
	}

	public Collection<Entity> searchByType(String tipo, String nut, String texto, int inicio, int total) throws Exception{


		String request =solr_url+"/select?fl=*,score&q="+getQuery(nut,null,texto)+"&start="+(inicio-1)+"&rows="+total+"&wt=json&fq=tipo:"+tipo;
		log.info(request);
		String response = this.restTemplate.getForObject(request, String.class);

		return parseResponse(response,null,null);
	}

	protected String formatRefCat(String texto){
		StringTokenizer token = new StringTokenizer(texto,"\\-");
		if (token.countTokens()==0){
			token = new StringTokenizer(texto,":");
		}
		if (token.countTokens()==6){
			String c1= token.nextToken();
			String c2= completeCode(token.nextToken(),3);
			String c3= completeCode(token.nextToken(),3);
			if (c3.equalsIgnoreCase("000")){
				c3=c2;
			}
			String c4= completeCode(token.nextToken(),2);
			String c5= completeCode(token.nextToken(),3);
			String c6= completeCode(token.nextToken(),5);
			return c1+c2+c3+c4+c5+c6;
		}
		return texto;
	}
	protected boolean esEntero(String texto){
		return texto.matches("[0-9]*");
		/*try{
			Integer.parseInt(texto);
		}
		catch(NumberFormatException ex){
			return false;
		}
		return true;*/
	}
	protected boolean empiezaPorNumero(String texto){
		return esEntero(texto.substring(0,1))||
				(texto.substring(0,1).equalsIgnoreCase("-")&& esEntero(texto.substring(1,2)));
	}
	protected String completeCode(String code, int length){
		String result = code;
		while (result.length()<length){
			result ="0"+result;
		}
		return result;
	}
	protected int numeroApariciones(String texto, String textoAContar){
		int count=0;
		int pos =texto.indexOf(textoAContar);
		while (pos>=0){
			count++;
			pos =texto.indexOf(textoAContar,pos+1);
		}
		return count;
	}
	protected String getQuery(String nut, Integer portal, String texto) throws Exception{
				texto=texto.replaceAll("-", "\\\\-").replaceAll("\\+", "\\\\+").replaceAll("&&", "\\\\&&").replaceAll("\\|\\|", "\\\\||").replaceAll("/", "\\\\/").
				replaceAll("!", "\\\\!").replaceAll("\\(", "\\\\(").replaceAll("\\)", "\\\\)").replaceAll("\\*", "\\\\*").replaceAll(":", "\\\\:").replaceAll("\"", "\\\\\"")
				.replaceAll("~", "\\\\~").replaceAll("\\?", "\\\\?");
		String codigo=texto;
		if(texto.equals("")){
			texto = "*";
			codigo=texto;
		}
		else if (empiezaPorNumero(texto)){  // empieza por n�mero y no tiene comas
			log.debug("Empiezap or numero");
			codigo = codigo.replaceAll(" ", "");  // eliminar todos los espacios en blanco
			if ((numeroApariciones(codigo,":")==5)||(numeroApariciones(texto,"-")==5)){ // podr�a ser parcela SIGPAC
				log.debug("5 separadores");
				
				String refcat = formatRefCat(codigo);
				log.debug(refcat);
				codigo="((\""+refcat+"\") || "+"(\""+codigo+"\"))";
				texto+=" "+ refcat;
			}
			else{
				codigo="\""+codigo+"\"";
			}
		}
		else{
			codigo="\""+codigo+"\"";
		}
		return "((codigo:"+codigo+"^5) || (nombre:("+texto+")^5)|| (nombre_prioritario:("+texto+")^5)|| (prefLabel:("+texto+")^5)|| (altLabel:("+texto+")^2)|| (keywords:("+texto+")^2) || (comment:("+texto+")^1)|| (texto_busqueda:("+texto+")^1) "+(portal!=null ? "|| portales:("+portal+")^10" :"")+") "+
		(nut!=null ? " && nut:("+nut+")" :"");
	}
	public String getFQ(String campo, String[] valores) throws Exception{
		String fq =campo+":(";
		boolean primero=true;
		for (String valor:valores){
			if (!primero){
				fq+=" || ";
			}
			fq+="\""+valor+"\"";
		}
		return fq+")";
	}
	
	public Collection<Entity> search(String nut, Integer portal, String texto, int inicio, int total,HashMap<String,TypeConfig> config_types,String[] tipos) throws Exception{
		String query=getQuery(nut,portal,texto);
		String fq=(tipos!=null?getFQ("facet_tipo",tipos):"");
		String request =solr_url+"/select?fl=*,score&sort=score DESC,prioridad asc&facet.field=facet_tipo&facet=on&fq={fq}&q={query}&start="+(inicio-1)+"&rows="+total+"&wt=json";
		log.info(request.replaceAll("fq",fq).replaceAll("query", query));
		String response = this.restTemplate.getForObject(request, String.class,fq,query);
	
		log.info(response);
		return parseResponse(response,config_types, portal);
	}
	
	public Collection<Entity> search(String nut, String texto, int inicio, int total,HashMap<String,TypeConfig> config_types,String[] tipos) throws Exception{
		return search(nut,null,texto,inicio,total,config_types,tipos);
	}
	
	public Collection<Entity> searchSolr( String texto, int inicio, int total,HashMap<String,TypeConfig> config_types) throws Exception{

		String request =solr_url+"/select?fl=*,score&facet.field=facet_tipo&facet=on&q="+texto+"&start="+(inicio-1)+"&rows="+total+"&wt=json";
		log.info(request);
		String response = this.restTemplate.getForObject(request, String.class);
		log.info(response);
		return parseResponse(response,config_types,null);
	}

	protected String getValue(JsonObject object, String propiedad){
		if (object.get(propiedad)!=null){
			return object.get(propiedad).getAsString();
		}
		else{
			return null;
		}
	}

	protected Double getDoubleValue(JsonObject object, String propiedadX){
		if (object.get(propiedadX)!=null){
			return object.get(propiedadX).getAsDouble();
		}
		else{
			return null;
		}
	}

	protected Integer getIntValue(JsonObject object, String propiedad){
		if (object.get(propiedad)!=null){

			return object.get(propiedad).getAsInt();

		}
		else{
			return null;
		}
	}

	 public int getNumFound(){
		 if (numFound==null){
			 return 0;
		 }
		 else{
			 return numFound;	 
		 }
		 
	 }
	 protected Collection<Entity> parseResponse(String res,HashMap<String,TypeConfig> config_types, Integer portal){
		 log.debug(res);
		 ArrayList<Entity> resultados = new ArrayList<Entity>();

		 JsonObject response = new JsonParser().parse(res).getAsJsonObject();
		 /*JsonArray counts = response.getAsJsonObject("facet_counts").getAsJsonObject("facet_fields").getAsJsonArray("tipo");
		log.debug("Procesando nº resultados por tipo");
		for(int i = 0;i<counts.size();i=i+2){
			String tipo = counts.get(i).getAsString();
			Entity entity = new Entity();
			entity.setType(tipo)
			entity,counts.get(i+1).getAsInt(),null);
			resultados.put(tipo, resultado);
		}*/
		 numFound = response.getAsJsonObject("response").get("numFound").getAsInt();
		 facets = response.getAsJsonObject("facet_counts").getAsJsonObject("facet_fields").toString();
		 JsonArray docs = response.getAsJsonObject("response").getAsJsonArray("docs");
		 log.debug("Procesando resultados");
		 for(int i = 0;i<docs.size();i++){
			 JsonObject object = docs.get(i).getAsJsonObject();
			 Entity entity = new Entity();
			 boolean esElPortal=false;
			 if ((portal != null)&&(object.get("portales")!=null)&&(object.get("portales").getAsJsonArray()!=null)){
				 log.debug("portal usuario"+portal);
				 JsonArray portales = object.get("portales").getAsJsonArray();
				 for (int j=0; j<portales.size();j++){
					 log.debug("portal "+portales.get(j).getAsString());
					 if (portales.get(j).getAsString().equalsIgnoreCase(portal.toString())){
						 esElPortal=true;
						 entity.setPortal(portal);
						 entity.setC_mun_via(object.get("c_mun_via").getAsString());
						 break;
					 }
				 }
			 }

			 String item_type=object.get("item_type").getAsString();
			 entity.setItemType(item_type);
			 entity.setScore(getDoubleValue(object,"score"));
			 ArrayList<String> tipoLabel = new ArrayList<String>();
			 ArrayList<String> tipo = new ArrayList<String>();
			 if (esElPortal){
log.debug("es el portal");
				 tipo.add("TroidesV");
				 entity.setTypeCode(tipo);
				 String nombre = getValue(object,"nombre");
				 int callefin = nombre.lastIndexOf("(");
				 if (callefin<=1){
					 callefin=nombre.length();
				 }
				 entity.setName(nombre.substring(0,callefin-1)+" "+portal+" "+nombre.substring(callefin));
			 }
			 else{
				 entity.setName(getValue(object,"nombre"));
				 JsonArray tipos = object.get("tipos").getAsJsonArray();

				 log.debug("tipos size "+tipos.size());
				 for (int j=0; j<tipos.size();j++){
					 tipo.add(tipos.get(j).getAsString());	
				 }
				 log.debug("meto tipo code "+tipo);
				 entity.setTypeCode(tipo);

				 if (object.get("tiposLabel")!=null){
					 JsonArray tiposLabel = object.get("tiposLabel").getAsJsonArray();

					 for (int j=0; j<tiposLabel.size();j++){
						 tipoLabel.add(tiposLabel.get(j).getAsString());	
					 }
				 }
			 }
			 if (tipoLabel.size()>0){
				 log.debug("meto tipo label "+tipoLabel);
				 entity.setType(tipoLabel);
			 }
			 else{ // si no tenemos etiquetas (de momento no hay para place) ponemos el tipo
				 ArrayList<String> etiquetas = new ArrayList<String>(); 
				 if((config_types != null) && (entity.getItemType().equalsIgnoreCase("Place"))){
					 log.debug("busco tipo label");
					 for (String type: entity.getTypeCode() ){
						 log.debug("busco tipo label:"+type);
						 TypeConfig config=config_types.get(type);
						 log.debug("busco tipo label:"+type);
						 if ((config!=null)&&(config.getEtiqueta()!=null)){
							 log.debug("meto tipo label:"+config.getEtiqueta());
							 etiquetas.add(config.getEtiqueta());
						 }
					 }

				 }
				 if (etiquetas.size()>0){
					 entity.setType(etiquetas);
				 }
				 else{
					 entity.setType(tipo);
				 }
			 }

			
		 
		 if (item_type.equalsIgnoreCase("Place")){
			 ArrayList<String> nut = new ArrayList<String>();
			 if (getValue(object,"municipio")!=null){
				 nut.add(getValue(object,"municipio"));
			 }
			 if (getValue(object,"localidad")!=null){
				 nut.add(getValue(object,"localidad"));
			 }
			 if (nut.size()>0){
				 entity.setNut(nut);
			 }
			 entity.setX(getDoubleValue(object,"xutm"));
			 entity.setY(getDoubleValue(object,"yutm"));
			 if (esElPortal){
				 entity.setId(getValue(object,"c_mun_via")+"_"+portal);
			 }
			 else{
				 entity.setId(getValue(object,"gid"));	 
			 }
			 entity.setC_muni_ine(getValue(object,"c_muni_ine"));
			 entity.setCode(getValue(object,"codigo"));
		 }
		 else{
			 if (object.get("nut")!=null){
				 JsonArray nuts = object.get("nut").getAsJsonArray();
				 ArrayList<String> nut = new ArrayList<String>();
				 for (int j=0; j<nuts.size();j++){
					 nut.add(nuts.get(j).getAsString());	
				 }
				 entity.setNut(nut);
			 }
			 entity.setId(getValue(object,"uri"));
			 String img_content=getValue(object,"img_contenturl");
			 if (img_content!=null){
				 log.debug("Hay imagen "+img_content);
				 Image img = new Image();
				 img.setContentUrl(img_content);
				 img.setLicense(getValue(object,"img_license"));
				 img.setUrl(getValue(object,"img_url"));
				 entity.setImage(img);
			 }
		 }
		 resultados.add(entity);

	 }
	 return resultados;
}


}
