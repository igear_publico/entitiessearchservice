package  es.ideariumConsultores.search.entities.client;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import es.ideariumConsultores.search.entities.Entity;
import es.ideariumConsultores.search.entities.SearchService;
import es.ideariumConsultores.search.entities.SparqlQuery;
import es.ideariumConsultores.search.entities.TypeConfig;

@Controller
public class SearchController {

    public final static Logger log = LoggerFactory.getLogger(SearchController.class.getName());


	@Value("${needEncoding}")
	String needEncoding;

	@Value("${sparql.url}")
	String sparql_ep;
	
	
	@Autowired
	SearchService searchService;
	
	@RequestMapping(value="/client/search")
	public String busquedaEnListado(Model m, @RequestParam String query, @RequestParam int start, @RequestParam int limit){
		if(needEncoding.equals("true")){
			try{
				query = new String(query.getBytes("ISO-8859-1"), "UTF-8");
			}catch (Exception e){}
		}
		
		m.addAttribute("query",query);
		
		try{
			Collection<Entity> resultados = searchService.buscar(query, start, limit,null,null);
			HashMap<String,TypeConfig> config = searchService.getTypeConfig();
			for (Entity resultado:resultados){
				for (String tipo:resultado.getType()){
					if (config.get(tipo)!=null){
						TypeConfig config_res = config.get(tipo);
						resultado.setIcon(config_res.getIcono());
						ArrayList<SparqlQuery> queries = config_res.getQueries();
						//String sparql_detail = "";
						String sparql_infobox = "";
						if (queries!=null){
						for (SparqlQuery sparql:queries){
							/*if (sparql.isSparql_detalle()){
								sparql_detail+="###"+sparql.getQuery().replaceAll("\n", " ").replaceAll("\\?s#", "<"+ resultado.getId()+">");
							}*/
							
								sparql_infobox+="\n"+sparql.getQuery().replaceAll("\\?s#", "<"+ resultado.getId()+">");
							
						}
						
						resultado.setSparql(sparql_infobox);
						}
					}
				}
				
			}
			
			m.addAttribute("resultados",resultados);
			int total=searchService.getNumFound();
		m.addAttribute("total",total);
		int paginasTotales = (int) Math.ceil((double)total / limit);
		
		m.addAttribute("paginas",paginasTotales);
		m.addAttribute("pagina",((start-1)/limit)+1);
		m.addAttribute("sparql_url",sparql_ep);


		}
		catch(Exception ex){
			log.error("Error buscando", ex);
			
		}

			
		return "busqueda";
	}
	@RequestMapping(value="/client/searchSolr")
	public String busquedaEnSolr(Model m, @RequestParam String query, @RequestParam int start, @RequestParam int limit){
		if(needEncoding.equals("true")){
			try{
				query = new String(query.getBytes("ISO-8859-1"), "UTF-8");
			}catch (Exception e){}
		}
		
		m.addAttribute("query",query);
		
		try{
			Collection<Entity> resultados = searchService.buscarSolr( query, start, limit,null);
			HashMap<String,TypeConfig> config = searchService.getTypeConfig();
			for (Entity resultado:resultados){
				for (String tipo:resultado.getType()){
					if (config.get(tipo)!=null){
						TypeConfig config_res = config.get(tipo);
						resultado.setIcon(config_res.getIcono());
						ArrayList<SparqlQuery> queries = config_res.getQueries();
						//String sparql_detail = "";
						String sparql_infobox = "";
						if (queries!=null){
						for (SparqlQuery sparql:queries){
							/*if (sparql.isSparql_detalle()){
								sparql_detail+="###"+sparql.getQuery().replaceAll("\n", " ").replaceAll("\\?s#", "<"+ resultado.getId()+">");
							}*/
							
								sparql_infobox+="\n"+sparql.getQuery().replaceAll("\\?s#", "<"+ resultado.getId()+">");
							
						}
						
						resultado.setSparql(sparql_infobox);
						}
					}
				}
				
			}
			
			m.addAttribute("resultados",resultados);
			int total=searchService.getNumFound();
		m.addAttribute("total",total);
		int paginasTotales = (int) Math.ceil((double)total / limit);
		
		m.addAttribute("paginas",paginasTotales);
		m.addAttribute("pagina",((start-1)/limit)+1);
		m.addAttribute("sparql_url",sparql_ep);


		}
		catch(Exception ex){
			log.error("Error buscando", ex);
			
		}

			
		return "busqueda";
	}
	@RequestMapping(value="/index")
	public String index(Model m){
		
			
		return "busqueda";
	}


}
