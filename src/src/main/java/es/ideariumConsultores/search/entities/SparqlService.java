package es.ideariumConsultores.search.entities;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SparqlService {
	  private final static Logger log = LoggerFactory.getLogger(SparqlService.class.getName());
	  
	@Value("${sparql.url}")
	String url_sparql;
	@Value("${virtuoso.grafo}")
	String grafo_virtuoso;

	public  ArrayList<String> getType(String sujeto) throws Exception{
		ArrayList<String> tipos=new ArrayList<String>();
		if (sujeto.equalsIgnoreCase("http://www.w3.org/2002/07/owl#Class")){
				tipos.add(sujeto);
				return tipos;
		}
	/*	log.debug("SELECT ?tipo "
				+ "FROM <"+grafo_virtuoso+"> WHERE {  <"+sujeto+"> a ?tipo.}");*/
		Query q =  QueryFactory.create("SELECT ?tipo "
				+ "FROM <"+grafo_virtuoso+"> WHERE {  <"+sujeto+"> a ?tipo.}");



		QueryExecution stmt = QueryExecutionFactory.sparqlService(url_sparql, q);

		ResultSet rs =  stmt.execSelect();
		
		while (rs.hasNext()){
			QuerySolution obj =rs.nextSolution();
			
			Iterator<String> vars = obj.varNames();

			while(vars.hasNext()){
				String var = vars.next();
				//log.debug(var);
				if(var.equalsIgnoreCase("tipo")){
					String tipo=null;
					if (obj.get("tipo").isLiteral()){
						tipo = obj.getLiteral(var).getString();
						
					}
					else{
						Resource tipo_uri = obj.getResource(var);
						tipo =	tipo_uri.getURI();
						
					}
					if (!tipo.equalsIgnoreCase(	"http://www.w3.org/2002/07/owl#NamedIndividual")){
						tipos.add(tipo);
					}
					
				}
			}
		}
		stmt.close();
		return tipos;

	}
	
	public  ArrayList<String> getRelObject(String sujeto, String relation) throws Exception{
	/*	log.debug("SELECT ?extId "
				+ " WHERE { <"+sujeto+"> <"+relation+"> ?extId}");*/
		Query q =  QueryFactory.create("SELECT ?extId "
				+ " WHERE { <"+sujeto+"> <"+relation+"> ?extId}");



		QueryExecution stmt = QueryExecutionFactory.sparqlService(url_sparql, q);

		ResultSet rs =  stmt.execSelect();
		ArrayList<String> ids=new ArrayList<String>();
		while (rs.hasNext()){
		//	log.debug("hasnext");
			QuerySolution obj =rs.nextSolution();
			
			Iterator<String> vars = obj.varNames();

			while(vars.hasNext()){
			//	log.debug("varnext");
				String var = vars.next();
			//log.debug(var);
				if(var.equalsIgnoreCase("extId")){
					if (obj.get("extId").isResource()){
						Resource id_uri = obj.getResource(var);
						
						ids.add(id_uri.getURI());
					}
				}
			}
		}
		stmt.close();
		return ids;

	}
}
