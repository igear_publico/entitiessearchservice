package  es.ideariumConsultores.search.entities;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;





@Service
public  class SearchService {
	@Autowired
	UtilsCoordTrans utilsCoordTrans;
	
	@Autowired
	SolrService solr;
	
	Integer numFound;
	String facets;
	
    @Autowired
    DataSource dataSource;

	
    @Autowired
    SparqlService sparqlService;
    
	  private final static Logger log = LoggerFactory.getLogger(SearchService.class.getName());
	/**
	
	/**
	 * Indica si el texto representa un n�mero entero
	 * @param texto
	 * @return
	 */
	static public boolean esEntero(String texto){
		return texto.matches("[0-9]*");
		/*try{
			Integer.parseInt(texto);
		}
		catch(NumberFormatException ex){
			return false;
		}
		return true;*/
	}

	/**
	 * Indica si el texto representa un n�mero real
	 * @param texto
	 * @return
	 */
	static protected boolean esReal(String texto){
		try{
			Double.parseDouble(texto);
		}
		catch(NumberFormatException ex){
			return false;
		}
		return true;
	}

	/**
	 * indica si el texto comienza por un n�mero
	 * @param texto
	 * @return
	 */
	static protected boolean empiezaPorNumero(String texto){
		return esEntero(texto.substring(0,1))||
				(texto.substring(0,1).equalsIgnoreCase("-")&& (texto.length()>1) && esEntero(texto.substring(1,2)));
	}

	/**
	 * indica si el texto es un c�digo que empieza por el prefijo indicado seguido de n�meros
	 * @param texto texto a comprobar si es c�digo
	 * @param prefijo prefijo por el que debe comenzar el texto para considerarlo c�digo
	 * @return true si texto empieza por el prefijo y el resto son n�meros, false en caso contrario
	 */
	static protected boolean esCodigo(String texto, String prefijo){
		if(texto.toUpperCase().startsWith(prefijo.toUpperCase())){
			return esEntero(texto.toUpperCase().replaceFirst(prefijo.toUpperCase(), ""));
		}
		return false;
	}

	/**
	 * Devuelve el n�mero de apariciones de una cadena de texto dentro de otra
	 * @param texto cadena de texto en la que contar
	 * @param textoAContar cadena de texto cuyas apariciones hay que contar
	 * @return n�mero de apariciones de textoAContar en texto
	 */
	static protected int numeroApariciones(String texto, String textoAContar){
		int count=0;
		int pos =texto.indexOf(textoAContar);
		while (pos>=0){
			count++;
			pos =texto.indexOf(textoAContar,pos+1);
		}
		return count;
	}

	/**
	 * Sustituye  el espacio en blanco por %20
	 * @param txt Cadena de texto a normalizar
	 * @return cadena sin espacios en blanco (sustituidos por %20)
	 */
	static protected String normalizaCadena(String txt, boolean replaceBlank){
		String txt_norm =txt;
		try{

			txt_norm =URLDecoder.decode(txt,"UTF-8");
		}
		catch(UnsupportedEncodingException ex){
			ex.printStackTrace();
		}
		txt_norm = txt_norm.replace("Ã¡", "a").replace("Ã©", "e").replace("Ã­", "i").replace("Ã³", "o").replace("Ãº", "u");
		txt_norm = txt_norm.replace("Ã�", "A").replace("Ã‰", "E").replace("Ã�", "I").replace("Ã“", "O").replace("Ãš", "U");
		txt_norm = txt_norm.replace("Ãœ", "U").replace("Ã¼", "u").replace("Ã±", "ny").replace("Ã‘", "NY").replace("Âº", "\u00BA");
		txt_norm = txt_norm.replace("&#201;", "E").replace("&#193;", "A").replace("&#205;", "I").replace("&#211;", "O").replace("&#218;", "U").replace("&#209;", "NY").replace("&#220;", "U");

		if (replaceBlank){
			txt_norm = txt_norm.replace(" ", "%20");
		}
		return txt_norm;

	}


	static protected String normalizaCadena(String txt){
		return normalizaCadena(txt,true);
	}

	static public String completeCode(String code, int length){
		String result = code;
		while (result.length()<length){
			result ="0"+result;
		}
		return result;
	}


 public int getNumFound(){
	 if (numFound==null){
		 return 0;
	 }
	 else{
		 return numFound;	 
	 }
	 
 }
 
 public Collection<Entity> buscarSolr(String txt, Integer beginRecord, Integer maxRecords,HashMap<String,TypeConfig> config_types) throws Exception{
		ArrayList<Entity> resultado = new ArrayList<Entity>();
		// Analizar la cadena para definir el tipo de b�squeda y realizar la b�squeda
		String texto=txt.trim();
		int total = (maxRecords != null? maxRecords:100);
		int inicio = (beginRecord != null ? beginRecord : 1);
		Collection<Entity> resultados=  solr.searchSolr(texto, inicio, total,config_types);
	
	numFound = solr.getNumFound();
	facets = solr.facets;
	return resultados;
 }
 
 protected boolean incluirTipo(String[] tipos,String eltipo){
	 if ((tipos==null)||(tipos.length==0)){
		 return true;
	 }
	 for (String tipo:tipos){
		 if (tipo.trim().equalsIgnoreCase(eltipo.trim())){
			 return true;
		 }
	 }
	 return false;
 }
	 public Collection<Entity> buscar(String txt, Integer beginRecord, Integer maxRecords,HashMap<String,TypeConfig> config_types, String facets_tipo) throws Exception{

		ArrayList<Entity> resultado = new ArrayList<Entity>();
		// Analizar la cadena para definir el tipo de b�squeda y realizar la b�squeda
		String texto=txt.trim();
		int total = (maxRecords != null? maxRecords:100);
		int inicio = (beginRecord != null ? beginRecord : 1);
		String[] tipos=null;
		if (facets_tipo!=null){
			tipos=facets_tipo.split(",");
		
		}
		if (txt.trim().length()==0){
			return solr.search(txt, inicio, total, config_types,tipos);
		}
		if (incluirTipo(tipos,"Coordenadas")&&((empiezaPorNumero(texto))&&((texto.indexOf(",")>0)||(texto.indexOf(";")>0)||(numeroApariciones(texto,":")==1))) ){  // empieza por n�mero y tiene alguna coma
			String separador = ",";
			if(texto.indexOf(";")>0){
				separador=";";
			}
			else if(texto.indexOf(":")>0){
				separador=":";
			}
			StringTokenizer token = new StringTokenizer(texto,separador);
			if(token.countTokens()==2){  // tiene una coma
				String parte1 = normalizaCadena(token.nextToken().trim(),false);
				String parte2 = normalizaCadena(token.nextToken().trim(),false);
				String huso="30";
				Double[] coords =null;
				String epsg="EPSG:25830";
				if ((parte1.replaceAll(" ", "").matches("-?([0-9]+\\u00BA)?([0-9]+\\u0027)?([0-9]+\\.?[0-9]*\\u0027\\u0027)?"))&&
						(parte2.replaceAll(" ", "").matches("-?([0-9]+\\u00BA)?([0-9]+\\u0027)?([0-9]+\\.?[0-9]*\\u0027\\u0027)?"))){ //coordenadas en grados minutos y segundos


					coords = new Double[]{utilsCoordTrans.getDecimalDegrees(parte1),
							utilsCoordTrans.getDecimalDegrees(parte2)};
					epsg = "EPSG:4258";
					try{
						coords = utilsCoordTrans.transform(coords,epsg,"EPSG:25830");
					}
					catch(Exception ex){
						log.error("No se pudieron transformar las coordenadas", ex);
					}

				}

				else {
					if (parte2.matches(".*[0-9]+[ ]?\\([0-9]+\\)")){


						huso = parte2.substring(parte2.indexOf("(")+1,parte2.indexOf(")"));
						parte2=parte2.substring(0, parte2.indexOf("(")-1).trim();

					}
					if (esReal(parte1)&&esReal(parte2)){  // son dos n�meros separados por coma
						coords = new Double[]{Double.parseDouble(parte1),Double.parseDouble(parte2)};
						if ((!huso.equalsIgnoreCase("30"))&&(!huso.equalsIgnoreCase("25830"))){
							if (huso.length() ==2)
							{
								huso = "258"+huso;
							}
							epsg = "EPSG:"+huso;
							try{
								coords = utilsCoordTrans.transform(coords,epsg,"EPSG:25830");
							}
							catch(Exception ex){
								log.error("No se pudieron transformar las coordenadas", ex);
							}
						}
						else{
							if ((coords[0]<=180)&&
									(coords[1]<=90)){
								epsg = "EPSG:4258";
								try{
									coords = utilsCoordTrans.transform(coords,epsg,"EPSG:25830");
								}
								catch(Exception ex){
									log.error("No se pudieron transformar las coordenadas", ex);
								}
							}
						}
					}

				}
				if (coords!=null){
					if ((coords[0]>=571580)&&(coords[0]<=812351)&&
							(coords[1]>=4412223)&&
							(coords[1]<=4756639)){
						Entity entity = new Entity();
						
						entity.setItemType("Place");
						entity.setScore(1.0);
						ArrayList<String> tipo = new ArrayList<String>();
							tipo.add("Coordenadas "+epsg);
							entity.setType(tipo);
							tipo = new ArrayList<String>();
							tipo.add("coordenadas");
							entity.setTypeCode(tipo);
						
						entity.setName(texto.replaceAll(separador, ":"));
						entity.setX(coords[0]);
						entity.setY(coords[1]);
						resultado.add(entity);
						numFound=1;
						facets = null;
					}
								
				
				}
			}
			return resultado;	
		}
		else{
			Collection<Entity> resultados;
			
			if (numeroApariciones(texto,",")==1){  // dos cadenas de texto separadas por coma
				// calle, municipio // localidad, municipio // toponimo, municipio
				StringTokenizer tokenizer = new StringTokenizer(texto,",");
				String token1=tokenizer.nextToken();
				String token2="";
				if (tokenizer.hasMoreTokens()){ // por si no hay nada detrás de la coma
					token2=tokenizer.nextToken();  // deber�a ser el municipio
				}
				
					token2=token2.trim();
					if (token1.trim().matches(".+[0-9]+")){  // es portal
						
						String portal = token1.substring(token1.lastIndexOf(" ")).trim();
						log.debug("es portal "+portal);
						resultados= solr.search(token2,new Integer(portal), token1.substring(0,token1.lastIndexOf(" ")).trim(), inicio, total, config_types,tipos);
					}
					else{
					resultados= solr.search(token2, token1, inicio, total, config_types,tipos);
					}


			}
			else { 
				if ((texto.matches(".+ [0-9]+"))&&(numeroApariciones(texto,",")==0)){
					String portal = texto.substring(texto.lastIndexOf(" ")).trim();
					log.debug("es portal "+portal);
					resultados=  solr.search(null,new Integer(portal),texto.substring(0,texto.lastIndexOf(" ")).trim(), inicio, total,config_types,tipos);
				}
				else{
				resultados=  solr.search(texto, inicio, total,config_types,tipos);
				}
				
			}
			facets = solr.facets;
			numFound = solr.getNumFound();
			return resultados;
		}
		
	}
		protected ArrayList<SparqlQuery> getSparqlQueries(Connection conn, String tipo, String filter) throws Exception{
			 
			 ArrayList<SparqlQuery> queries = new ArrayList<SparqlQuery>();
			 try{

				 Statement stmt_queries = conn.createStatement();
				 log.debug("select * from consultas_sparql where (tipo ='"+tipo+"' or tipo is null) and "+filter+" order by orden");
				 ResultSet rs_queries = stmt_queries.executeQuery("select * from consultas_sparql where (tipo ='"+tipo+"' or tipo is null) and "+filter+" order by orden");
				
				 while (rs_queries.next()){
					 SparqlQuery query = new SparqlQuery();
				//	 log.debug(rs_queries.getString("pk"));
					 query.setQuery(rs_queries.getString("sparql"));
					query.setSparql_url(rs_queries.getString("sparql_url"));
					query.setRelacion(rs_queries.getString("relacion"));
					 query.setComponente(rs_queries.getString("componente"));
					// query.setColumna(rs_queries.getString("columna"));
					 queries.add(query);
				 }
				 stmt_queries.close();
								 
			 }
			 catch(Exception ex){
				 log.error("Error obteniendo consultas Sparql del tipo "+tipo, ex);
			 }
			 return queries;
		}
		
		public ArrayList<SparqlQuery> getSparqlQueries(String tipo, String filter) throws Exception{
			 Connection conn =dataSource.getConnection();
			
			 try{

				return getSparqlQueries(conn,tipo, filter);
								 
			 }
			 finally{
				 conn.close();
			 }
			 
		}
	public HashMap<String,TypeConfig> getTypeConfig() throws Exception{
		 HashMap<String,TypeConfig> config_types = new  HashMap<String,TypeConfig> ();
		 Connection conn =dataSource.getConnection();
		 try{
		 Statement stmt = conn.createStatement();
		 ResultSet rs = stmt.executeQuery("select * from config_busquedas");
		 while (rs.next()){
			 TypeConfig config = new TypeConfig();
			 config.setIcono(rs.getString("icono"));
			
			 ArrayList<SparqlQuery> queries = getSparqlQueries(conn,rs.getString("tipo"),"infobox");
			 config.setQueries(queries);
			 config_types.put(rs.getString("tipo"), config);
			 
		 }
		 log.debug("select * from config_busquedas_place");
		 rs = stmt.executeQuery("select * from config_busquedas_place");
		 while (rs.next()){
			 TypeConfig config = new TypeConfig();
			 config.setIcono(rs.getString("icono"));
			 config.setEtiqueta(rs.getString("etiqueta"));
			 log.debug(rs.getString("tipo"));
			config.setWms(rs.getString("wms"));
			config.setWfs(rs.getString("wfs"));
			config.setVisor(rs.getString("visor"));
			config.setCartoteca(rs.getString("cartoteca"));
			config.setMunicipio(rs.getString("municipio"));
			config.setDescargas(rs.getString("descargas"));
			config.setDondevivo(rs.getString("dondevivo"));
			config.setRjt(rs.getString("rjt"));
			config.setGeoboa(rs.getString("geoboa"));
			 config_types.put(rs.getString("tipo"), config);
			 
		 }
		 }
		 catch(Exception ex){
			 log.error("Error obteniendo configuración de tipos", ex);
		 }
		 finally{
			 conn.close();
		 }
		 return config_types;
	}
	
	public String getTypes() throws Exception{
		String tipos="";
		 Connection conn =dataSource.getConnection();
		 try{
		 Statement stmt = conn.createStatement();
		 ResultSet rs = stmt.executeQuery("select tipo from codfacetstipo order by orden");
		 while (rs.next()){
			tipos+=(tipos.length()>0?",":"")+"\""+rs.getString("tipo")+"\"";
			 
		 }
		  }
		 finally{
			 conn.close();
		 }
		 return "["+tipos+"]";
	}
	public String getJSON(Collection<Entity> resultados, String format,HashMap<String,TypeConfig> config_types){
		//log.debug("json");
		
		
		StringBuffer sb = new StringBuffer();
		sb.append("{\"@context\": {"+
   "\"@vocab\": \"http://schema.org/\","+
   "\"goog\": \"http://schema.googleapis.com/\","+
   " \"resultScore\": \"goog:resultScore\","+
   " \"detailedDescription\": \"goog:detailedDescription\","+
   " \"EntitySearchResult\": \"goog:EntitySearchResult\","+
   " \"kg\": \"http://g.co/kg\""+
 " },");
  		sb.append("\"@type\": \"ItemList\",");
  		sb.append("\"numFound\": "+getNumFound()+",");
  		if (format!=null && format.equalsIgnoreCase("custom") && (facets!=null)){
  			sb.append("\"facets\": "+facets+",");
  		}
		sb.append("\"itemListElement\": [");
		boolean primero=true;
		for (Entity entity:resultados){
			if (!primero){
				sb.append(",");
			}
			primero=false;
			sb.append(" { \"@type\":\""+entity.getItemType()+"\",\"result\": " );
			
			sb.append(entity.toString());
			if (format!=null && format.equalsIgnoreCase("custom")){
			boolean hayTipo=false;
			if (config_types!=null){
				for (String tipo :entity.getTypeCode()){
					//log.debug("tipo: "+tipo);
					if(config_types.get(tipo)!=null){
						//log.debug("hay tipo conf");
						TypeConfig config = config_types.get(tipo);
						config.setTipo(entity.getTypeCode());
						ArrayList<SparqlQuery> queries= config.getQueries();
						try{
						 queries = getExternalQueries(config.getQueries(),entity.getId());
						
						}
						catch(Exception ex){
							log.error("Error obteniendo consultas externas", ex);
						}
						ArrayList<SparqlQuery> queries_bak=config.getQueries();
						config.setQueries(queries);
						sb.append(",\"displayInfo\":"+config.toString(entity));
						hayTipo=true;
						config.setQueries(queries_bak);
						//log.debug("original3 "+config.getQueries());
						break;
					}
					
				}
			}
			if (!hayTipo){

					sb.append(",\"displayInfo\":{\"tipo\":[");
					int num=0;
					for (String tipo :entity.getTypeCode()){
						
						sb.append((num>0?",":"")+"\""+tipo+"\"");
						num++;
					}
					sb.append("]}");

			}
			}
			sb.append(", \"resultScore\":"+ entity.getScore()+"}");
	    }
		
		sb.append("]}");
		return sb.toString();
	}



public ArrayList<SparqlQuery> getExternalQueries(ArrayList<SparqlQuery> queries, String sujeto) throws Exception{
	ArrayList<SparqlQuery> sparql=new ArrayList<SparqlQuery>();
	if (queries!=null){
//	log.debug("setExternalqueries"+sparql.size());
	HashMap<String,ArrayList<String>> external_ids=new HashMap<String,ArrayList<String>> ();
	for (int j=0; j<queries.size(); j++){
		SparqlQuery  query = queries.get(j).getACopy();
	//	log.debug(query.getComponente());
		if (query.getRelacion()!=null){
			//log.debug("relacion "+query.getRelacion());
			if (external_ids.get(query.getRelacion())==null){
				external_ids.put(query.getRelacion(), sparqlService.getRelObject(sujeto,query.getRelacion()));
				
			}
			ArrayList<String> ids = external_ids.get(query.getRelacion());
			if (ids!=null && ids.size()>0){
				//log.debug("ids "+ids.size());
				String query_sparql="";
				for (int i=0; i<ids.size();i++){
					//log.debug("id"+i+" "+ids.get(i));
					if (i==0){
						query_sparql=query.getQuery();
					//	log.debug("qs orig "+query_sparql);
					query.setQuery(query.getQuery().replaceAll("\\?s#","<"+ ids.get(i)+">"));
					//log.debug("qs "+query_sparql);
					//log.debug(query.getQuery());
					sparql.add(query);
					}
					else{
						SparqlQuery new_query = query.getACopy();
						//log.debug("qs orig"+query_sparql);
						new_query.setQuery(query_sparql.replaceAll("\\?s#","<"+ ids.get(i)+">"));
						//log.debug(new_query.getQuery());
						sparql.add(new_query);
						
					}
				}
			}
			
		}
		else{
			sparql.add(query);
		}
	}
	}
	return sparql;
}
}
