package  es.ideariumConsultores.search.entities;


import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.annotations.Expose;

public class TypeConfig {


	@Expose
    private ArrayList<String> tipo;


	@Expose
    private String icono;

	 private String etiqueta;;

	 
	 @Expose
	 private String wms;
	 @Expose
	 private String wfs;
	 @Expose
	 private String visor;
	 @Expose
	 private String cartoteca;
	 @Expose
	 private String dondevivo;
	 @Expose
	 private String descargas;
	 @Expose
	 private String rjt;
	 @Expose
	 private String geoboa;
	 @Expose
	 private String municipio;
	@Expose
 ArrayList<SparqlQuery> queries;





	public String getMunicipio() {
		return municipio;
	}



	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}



	public String getVisor() {
		return visor;
	}



	public void setVisor(String visor) {
		this.visor = visor;
	}



	public String getCartoteca() {
		return cartoteca;
	}



	public void setCartoteca(String cartoteca) {
		this.cartoteca = cartoteca;
	}



	public String getDondevivo() {
		return dondevivo;
	}



	public void setDondevivo(String dondevivo) {
		this.dondevivo = dondevivo;
	}



	public String getDescargas() {
		return descargas;
	}



	public void setDescargas(String descargas) {
		this.descargas = descargas;
	}



	public String getRjt() {
		return rjt;
	}



	public void setRjt(String rjt) {
		this.rjt = rjt;
	}



	public String getGeoboa() {
		return geoboa;
	}



	public void setGeoboa(String geoboa) {
		this.geoboa = geoboa;
	}



	public String getEtiqueta() {
		return etiqueta;
	}



	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}



	public String getWms() {
		return wms;
	}



	public void setWms(String wms) {
		this.wms = wms;
	}



	public String getWfs() {
		return wfs;
	}



	public void setWfs(String wfs) {
		this.wfs = wfs;
	}



	public ArrayList<String> getTipo() {
		return tipo;
	}



	public void setTipo(ArrayList<String> tipo) {
		this.tipo = tipo;
	}



	public String getIcono() {
		return icono;
	}



	public void setIcono(String icono) {
		this.icono = icono;
	}



	public ArrayList<SparqlQuery> getQueries() {
		return queries;
	}



	public void setQueries(ArrayList<SparqlQuery> queries) {
		this.queries = queries;
	}

    public String toString(String id) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        
      return gson.toJson(this).replaceAll("\\?s#", id);
    }
    public String toString(Entity entity) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        String json = gson.toJson(this);
        String id =entity.getId();
        if (id!=null){
    		if (entity.getItemType().equalsIgnoreCase("Entity")){
    			id = "<"+ id+">";
    		}

        	json = json.replaceAll("\\?s#", id);
        }
if (entity.getItemType().equalsIgnoreCase("Place")){
        if (entity.getCode()!=null){
        	json = json.replaceAll("\\?codigo#", entity.getCode());
        }
        if (entity.getC_muni_ine()!=null){
        	json = json.replaceAll("\\?c_muni_ine#", entity.getC_muni_ine());
        }
        if (entity.getX()!=null){
        	json = json.replaceAll("\\?x#", entity.getX().toString());
        }
        if (entity.getY()!=null){
        	json = json.replaceAll("\\?y#", entity.getY().toString());
        }
        if (entity.getC_mun_via()!=null){
        	json = json.replaceAll("\\?c_mun_via#", entity.getC_mun_via().toString());
        }
        if (entity.getPortal()!=null){
        	json = json.replaceAll("\\?portal#", entity.getPortal().toString());
        }
}
      return json;
    }

   }
