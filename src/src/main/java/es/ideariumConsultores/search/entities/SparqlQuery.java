package  es.ideariumConsultores.search.entities;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

public class SparqlQuery implements Cloneable{


	    
@Expose
    private String query;


@Expose
private String sparql_url;


private String relacion;


@Expose
private String componente;




	public String getComponente() {
	return componente;
}


public SparqlQuery getACopy() throws Exception{
	return (SparqlQuery) this.clone();
}
public void setComponente(String componente) {
	this.componente = componente;
	
}



	public String getQuery() {
		return query;
	}



	public void setQuery(String query) {
		this.query = query;
	}




	public String getSparql_url() {
		return sparql_url;
	}



	public void setSparql_url(String sparql_url) {
		this.sparql_url = sparql_url;
	}






	public String getRelacion() {
		return relacion;
	}


	public void setRelacion(String relacion) {
		this.relacion = relacion;
	}


	@Override
    public String toString() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

      return gson.toJson(this);
    }



   }
