package  es.ideariumConsultores.search.entities;



import static org.springframework.http.HttpStatus.OK;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.jena.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@org.springframework.web.bind.annotation.RestController
public class RestController {


	@Value("${logging.path}")
	String logpath;

	@Value("${needEncoding}")
	String needEncoding;

	@Autowired
	SearchService searchService;

	@Autowired
	SparqlService sparqlService;
	
	private final static Logger log = LoggerFactory.getLogger(RestController.class.getName());



	@RequestMapping(value = "/logs", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity logs() throws Exception {
		try{
		byte[] encoded = Files.readAllBytes(Paths.get(logpath+"spring.log"));
		String s = new String(encoded, StandardCharsets.US_ASCII);
		return ResponseEntity.status(OK).body(s);
		}
		catch(Exception ex){
			log.error("Error recuperando log "+logpath+"spring.log",ex);
			return ResponseEntity.status(org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getLocalizedMessage());
		}
		
	}

	
	@RequestMapping(value = "/search", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity search(@RequestParam(value = "query",required = true) String query, @RequestParam(value = "facetTipo",required = false) String facets_tipo,@RequestParam(value = "limit", required = false) Integer limit,
			@RequestParam(value = "start", required = false) Integer start, @RequestParam(value = "types", required = false) String[] types, @RequestParam(value = "format", required = false) String format) throws Exception {
		try{
			if(needEncoding.equals("true")){
				try{
					query = new String(query.getBytes("ISO-8859-1"), "UTF-8");
					if (facets_tipo != null){
						facets_tipo = new String(facets_tipo.getBytes("ISO-8859-1"), "UTF-8");
					}
				}catch (Exception e){}
			}
			HashMap<String,TypeConfig> config_types=null;
			if (format!=null && format.equalsIgnoreCase("custom")){
				try{
				config_types=searchService.getTypeConfig();
				}
				catch(Exception ex){
					 log.error("Error obteniendo configuración de tipos", ex);
				}
			}
			
			Collection<Entity> resultados = searchService.buscar(query, start, limit,config_types,facets_tipo);
			return ResponseEntity.status(OK).body(searchService.getJSON(resultados,format,config_types));
		

		}
		catch(Exception ex){
			log.error("Error buscando", ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}


	}
	
	@RequestMapping(value = "/entity", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity entity( @RequestParam(value = "subject", required = true) String sujeto) throws Exception {
		try{
			if(needEncoding.equals("true")){
				try{
					sujeto = new String(sujeto.getBytes("ISO-8859-1"), "UTF-8");
				}catch (Exception e){}
			}
			ArrayList<String> tipos = sparqlService.getType(sujeto);
			
			ArrayList<SparqlQuery> sparql = null;
			int num_queries=0;
			for (String tipo:tipos){
				ArrayList<SparqlQuery> sparql_tmp = searchService.getSparqlQueries(tipo,"detalle");
				 if (sparql_tmp!=null){
					 if (sparql_tmp.size()>num_queries){
						 num_queries=sparql_tmp.size();
						 sparql=sparql_tmp;
					 }
					 
				 }
			}
			sparql = searchService.getExternalQueries(sparql,sujeto);
			/*HashMap<String,ArrayList<String>> external_ids=new HashMap<String,ArrayList<String>> ();
			for (int j=0; j<sparql.size(); j++){
				SparqlQuery  query = sparql.get(j); 
				if (query.getRelacion()!=null){
					
					if (external_ids.get(query.getRelacion())==null){
						external_ids.put(query.getRelacion(), sparqlService.getRelObject(sujeto,query.getRelacion()));
						
					}
					ArrayList<String> ids = external_ids.get(query.getRelacion());
					if (ids!=null){
						String query_sparql="";
						for (int i=0; i<ids.size();i++){
							log.debug("id"+i+" "+ids.get(i));
							if (i==0){
								query_sparql=query.getQuery();
								log.debug("qs "+query_sparql);
							query.setQuery(query.getQuery().replaceAll("\\?s#","<"+ ids.get(i)+">"));
							log.debug("qs "+query_sparql);
							log.debug(query.getQuery());
							}
							else{
								SparqlQuery new_query = query.getACopy();
								log.debug("qs"+query_sparql);
								new_query.setQuery(query_sparql.replaceAll("\\?s#","<"+ ids.get(i)+">"));
								log.debug(new_query.getQuery());
								sparql.add(j, new_query);
								j++;
							}
						}
					}
				}
			}*/
			String json = sparql.toString().replaceAll("\\?s#","<"+ sujeto+">");
			return ResponseEntity.status(OK).body(json);
		

		}
		catch(Exception ex){
			log.error("Error buscando", ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}


	}
	@RequestMapping(value = "/types", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity types( ) throws Exception {
		try{
			
						return ResponseEntity.status(OK).body(searchService.getTypes());
		

		}
		catch(Exception ex){
			log.error("Error obteniendo tipos", ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}


	}
}

