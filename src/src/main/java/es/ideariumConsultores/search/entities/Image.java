package  es.ideariumConsultores.search.entities;


import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.annotations.Expose;

public class Image {


	    

    @Expose
    private String contentUrl;

    @Expose
    private String url;

        
    @Expose
    private String license;


   




	public String getContentUrl() {
		return contentUrl;
	}







	public void setContentUrl(String contentUrl) {
		this.contentUrl = contentUrl;
	}







	public String getUrl() {
		return url;
	}







	public void setUrl(String url) {
		this.url = url;
	}







	public String getLicense() {
		return license;
	}







	public void setLicense(String license) {
		this.license = license;
	}







	@Override
    public String toString() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

      return gson.toJson(this);
    }



   
}
