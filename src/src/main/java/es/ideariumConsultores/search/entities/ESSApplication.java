package  es.ideariumConsultores.search.entities;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
public class ESSApplication extends SpringBootServletInitializer {




	public static void main(String[] args) throws Exception {
		SpringApplication.run(ESSApplication.class, args);
	}



}


