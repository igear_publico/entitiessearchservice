package  es.ideariumConsultores.search.entities;


import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.annotations.Expose;

public class Entity {


	    private String itemType;
	    private Double score;
	 
    @Expose
    private ArrayList<String> type;

    @Expose
    private String name;

    @Expose
    private ArrayList<String> nut;

    @Expose
    private Double x;

    @Expose
    private Integer portal;
    
    @Expose
    private String c_mun_via;
    
    @Expose
    private Double y;

    @Expose
    private String id;


    private String c_muni_ine;
    @Expose
    private String code;

    @Expose
    private Image image;

    private String icon;
    private String sparql;

    private ArrayList<String> typeCode;
	

	public ArrayList<String> getType() {
		return type;
	}






	public void setType(ArrayList<String> type) {
		this.type = type;
	}











	public Integer getPortal() {
		return portal;
	}






	public void setPortal(Integer portal) {
		this.portal = portal;
	}






	public String getC_mun_via() {
		return c_mun_via;
	}






	public void setC_mun_via(String c_mun_via) {
		this.c_mun_via = c_mun_via;
	}






	public String getC_muni_ine() {
		return c_muni_ine;
	}






	public void setC_muni_ine(String c_muni_ine) {
		this.c_muni_ine = c_muni_ine;
	}






	public ArrayList<String> getTypeCode() {
		return typeCode;
	}






	public void setTypeCode(ArrayList<String> typeCode) {
		this.typeCode = typeCode;
	}






	public String getItemType() {
		return itemType;
	}






	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public Image getImage() {
		return image;
	}






	public void setImage(Image image) {
		this.image = image;
	}






	public Double getX() {
		return x;
	}






	public void setX(Double x) {
		this.x = x;
	}






	public Double getY() {
		return y;
	}






	public void setY(Double y) {
		this.y = y;
	}








	public String getName() {
		return name;
	}






	public void setName(String name) {
		this.name = name;
	}









	public String getId() {
		return id;
	}






	public void setId(String id) {
		this.id = id;
	}






	public String getCode() {
		return code;
	}






	public void setCode(String code) {
		this.code = code;
	}











	public ArrayList<String> getNut() {
		return nut;
	}






	public void setNut(ArrayList<String> nut) {
		this.nut = nut;
	}






	public Double getScore() {
		return score;
	}






	public void setScore(Double score) {
		this.score = score;
	}






	public String getIcon() {
		return icon;
	}






	public void setIcon(String icon) {
		this.icon = icon;
	}








	public String getSparql() {
		return sparql;
	}






	public void setSparql(String sparql) {
		this.sparql = sparql;
	}






	@Override
    public String toString() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

      return gson.toJson(this).replaceAll("\"id\":","\"@id\":").replaceAll("\"name\":","\"@name\":");
    }



   
}
