package  es.ideariumConsultores.search.entities;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UtilsCoordTrans {
	

    @Autowired
    DataSource dataSource;
    
    private final static Logger log = LoggerFactory.getLogger(UtilsCoordTrans.class.getName());
    
	public  String getURN(String srs) {
		return srs.replaceAll("EPSG", "urn:epsg:crs");
		
	}
	protected  String getQueryWCTS_XML(String  srsOrigen,String srsDestino, Double  coordX,Double  coordY) {
		String content = "";
		content += "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n";
		content += "<wps:Execute service=\"WPS\" version=\"1.0.0\" xmlns:wps=\"http://www.opengis.net/wps/1.0.0\" xmlns:ows=\"http://www.opengis.net/ows/1.1\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opengis.net/wps/1.0.0 http://schemas.opengis.net/wps/1.0.0/wpsExecute_request.xsd\">\n";
		content += "<ows:Identifier>TransformCoordinates</ows:Identifier>\n";
		content += "<wps:DataInputs>\n";
		content += "<wps:Input>\n";
		content += "<ows:Identifier>SourceCRS</ows:Identifier>\n";
		content += "<wps:Data>\n";
		content += "<wps:LiteralData>"+srsOrigen.toLowerCase()+"</wps:LiteralData>\n";
		content += "</wps:Data>\n";
		content += "</wps:Input>\n";
		content += "<wps:Input>\n";
		content += "<ows:Identifier>TargetCRS</ows:Identifier>\n";
		content += "<wps:Data>\n";
		content += "<wps:LiteralData>"+srsDestino.toLowerCase()+"</wps:LiteralData>\n";
		content += "</wps:Data>\n";
		content += "</wps:Input>\n";
		content += "<wps:Input>\n";
		content += "<ows:Identifier>InputData</ows:Identifier>\n";
		content += "<wps:Data>\n";
		content += "<wps:ComplexData mimeType=\"text/xml; subtype=gml/3.1.1\">\n";
		content += "<gml:FeatureCollection xmlns:gml=\"http://www.opengis.net/gml\" xmlns:wfs=\"http://www.opengis.net/wfs\" xmlns:p=\"http://example.org\">\n";
		content += "<gml:featureMember>\n";
		content += "<p:Point>\n";
		content += "<gml:pointProperty>\n";
		content += "<gml:Point srsName=\"" + srsOrigen + "\">\n";
		content += "<gml:pos srsDimension=\"2\">" + coordX + " " + coordY + "</gml:pos>\n";
		content += "</gml:Point>\n";
		content += "</gml:pointProperty>\n";
		content += "</p:Point>\n";
		content += "</gml:featureMember>\n";
		content += "</gml:FeatureCollection>\n";
		content += "</wps:ComplexData>\n";
		content += "</wps:Data>\n";
		content += "</wps:Input>\n";
		content += "</wps:DataInputs>\n";
		content += "<wps:ResponseForm>\n";
		content += "<wps:RawDataOutput mimeType=\"text/xml; subtype=gml/3.1.1\">\n";
		content += "<ows:Identifier>TransformedData</ows:Identifier>\n";
		content += "</wps:RawDataOutput>\n";
		content += "</wps:ResponseForm>\n";
		content += "</wps:Execute>\n";

		return content;
	}
	public  double getDecimalDegrees(String gradosMinSeg){
		
		int posD = gradosMinSeg.indexOf("\u00BA");
		int posM = gradosMinSeg.indexOf("'");
		int posS = gradosMinSeg.indexOf("''");
		if (posM==posS){
			posM=-1;
		}
		double grados = 0;
		if(posS>0){
			int pos_ini=posM;
			if (pos_ini<0){
				pos_ini=posD;
			}
			grados +=Double.parseDouble(gradosMinSeg.substring(pos_ini+1,posS))/60.0;
		}
		if(posM>0){
		grados = (grados + Double.parseDouble(gradosMinSeg.substring(posD+1,posM)))/60.0;
		}
		else{
			grados = grados/60.0;
		}
		if(posD>0){
		 grados += Double.parseDouble(gradosMinSeg.substring(0,posD));
		}
		
		if (gradosMinSeg.substring(0,1).equalsIgnoreCase("-")){
			grados = 0-grados;
		}
		
		return grados;
	}
	public  Double[]  transform(Double[] coords, String  srsOrigen,String srsDestino)throws Exception{
		/*String reply = BusquedaSimple.doSearch(properties.getProperty("WCTS_URL"),getQueryWCTS_XML(srsOrigen, srsDestino,coords[0],coords[1]));
		return processResult_WCTS(reply);*/
		
		Connection conn =null;
		try{
		 conn = dataSource.getConnection();
		}
		catch(Exception ex){
			log.error("No se pudo conectar con la base de datos", ex);
			return null;
		}
		Statement sentencias=null;
		try{
		sentencias= conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		}
		catch(Exception ex){
			log.error("Error al crear statement ", ex);
			conn.close();
			return null;
		}
	
		ResultSet rs =null;
		log.info("select st_x(punto) as x, st_y(punto) as y from (SELECT st_transform(ST_GeomFromText('POINT("+coords[0]+" "+coords[1]+")',"+srsOrigen.toLowerCase().replaceAll("epsg:", "")+"),"+srsDestino.toLowerCase().replaceAll("epsg:", "")+") as punto)T");
		rs= sentencias.executeQuery("select st_x(punto) as x, st_y(punto) as y from (SELECT st_transform(ST_GeomFromText('POINT("+coords[0]+" "+coords[1]+")',"+srsOrigen.toLowerCase().replaceAll("epsg:", "")+"),"+srsDestino.toLowerCase().replaceAll("epsg:", "")+") as punto)T");
		if (rs.next()){
			return new Double[]{rs.getDouble("x"),rs.getDouble("y")};
		}
		conn.close();
		return null;
	}
	
	
	protected   Double[] processResult_WCTS(String theReply) throws Exception {
		String selectedData = "";
		String reply = theReply;

		int pos = reply.indexOf("<gml:pos>", 1);
		if (pos != -1) {
			int startpos = pos + 9;
			int endpos = reply.indexOf("</gml:pos>",startpos);
			selectedData = reply.substring(startpos,endpos);
			/*int  pos2 = selectedData.indexOf("ts=\" \">", 1);
			if (pos2 != -1) {
				int startpos2 = pos2 + 7;
				endpos = selectedData.indexOf("</gml:pos>", startpos2);
				String selectedData2 = selectedData.substring(startpos2,endpos);*/

				int commapos = selectedData.indexOf(' ',0);

				if (selectedData != "") {
					String xWCTS = selectedData.substring(0, commapos);
					String yWCTS = selectedData.substring(commapos+1);
					return new Double[]{new Double(Double.parseDouble(xWCTS)),new Double(Double.parseDouble(yWCTS))};
				}
			//}
		}
		throw new Exception("No se pudieron transformar las coordenadas. Respuesta de WCTS:"+theReply);
		
	}
}
