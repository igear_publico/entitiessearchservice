# EntitiesSearchService


### Dependencias

Para el funcionamiento del servicio EntitiesSearchService es necesario disponer de:

- tablas de configuración, según el modelo ER de la carpeta doc, almacenadas en PostgreSQL
- Solr sobre el que realizar las búsquedas, en el que habrá documentos de tipo Entity (correspondientes a elementos del grafo) y de tipo Place (features no almacenadas en el grafo)
- punto SPARQL para consulta del grafo


### Instalación

Para instalarlo, generar el war y desplegar en el servidor de aplicaciones
